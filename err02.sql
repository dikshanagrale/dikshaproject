

DROP PROCEDURE IF EXISTS sp_name;

DELIMITER $$

CREATE PROCEDURE sp_name(p_id INT, p_name VARCHAR(40),p_price FLOAT )
BEGIN

          DECLARE v_err INT DEFAULT 0;
          DECLARE COUNTINUE HANDLER FOR 1062 SET v_err = 1;

          INSERT INTO ADDBOOK(id,name,price) VALUES (p_id, p_name, p_price);

           IF v_err = 0 THEN
                  SELECT 'Book added' AS status;
          ELSE
                  SELECT 'Book add failed' AS status;
          END IF;

   
END;
$$

DELIMITER ;

